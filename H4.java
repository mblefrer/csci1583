import java.util.Scanner;//input
public class H4{
	
	final static int NORTH = 0;
	final static int EAST =  1;
	final static int WEST = 2;
	final static int SOUTH = 3;
	
	static String [] roomDescription =  new String[7];
	static int [] [] exits = new int [7] [4];
	
	
	public static void main(String[] args){
		rooms();
		
		Scanner input = new Scanner(System.in);
		
		int room = 0;
		
		String choice = "";
		
		while(choice.compareToIgnoreCase("q") != 0){
			System.out.println(roomDescription[room]);
			System.out.println("Type 'N' to go north");
			System.out.println("Type 'S' to go south");
			System.out.println("Type 'E' to go east");
			System.out.println("Type 'W' to go west");
			choice = input.nextLine();
			
			if(choice.compareToIgnoreCase("N") == 0){
				if(exits [room][NORTH] != -1){
					room = exits [room] [NORTH];
				} else {
					System.out.println("There is no room in that direction");
				}
			}//end north if
			
			if(choice.compareToIgnoreCase("S") == 0){
				if(exits [room] [SOUTH] != -1){
					room = exits [room] [SOUTH];
				} else {
					System.out.println("There is no room in that direction");
				}
			}//end south if
			
			if(choice.compareToIgnoreCase("E") == 0){
				if(exits [room] [EAST] != -1){
					room = exits [room] [EAST];
				} else {
					System.out.println("There is no room in that direction");
				}
			}//end east if
			
			if(choice.compareToIgnoreCase("W") == 0){
				if(exits [room] [WEST] != -1){
					room = exits [room] [WEST];
				} else{
					System.out.println("There is no room in this direction");
				}
			}
		}//endwhile
	}//end of main
	
	public static void rooms(){
		
		roomDescription [0] = "You are in the stairwell";
		roomDescription [1] = "You are in the North Hall";
		roomDescription [2] = "You are in the South Hall";
		roomDescription [3] = "You are in the second bedroom";
		roomDescription [4] = "You are in the dining room";
		roomDescription [5] = "You are in the first bedroom";
		roomDescription [6] = "You are in the kitchen";
		
		exits [0] [NORTH] = -1;
		exits [0] [SOUTH] = 1;
		exits [0] [EAST] = -1;
		exits [0] [WEST] = -1;
		
		exits [1] [NORTH] = 0;
		exits [1] [SOUTH] = 2;
		exits [1] [EAST] = 6;
		exits [1] [WEST] = 5;
		
		exits [2] [NORTH] = 1;
		exits [2] [SOUTH] = -1;
		exits [2] [EAST] = 4;
		exits [2] [WEST] = 3;
		
		exits [3] [NORTH] = 5;
		exits [3] [SOUTH] = -1;
		exits [3] [EAST] = 2;
		exits [3] [WEST] = -1;
		
		exits [4] [NORTH] = 6;
		exits [4] [SOUTH] = -1;
		exits [4] [EAST] = -1;
		exits [4] [WEST] = 2;
		
		exits [5] [NORTH] = -1;
		exits [5] [SOUTH] = 3;
		exits [5] [EAST] = 1;
		exits [5] [WEST] = -1;
		
		exits [6] [NORTH] = -1;
		exits [6] [SOUTH] = 4;
		exits [6] [EAST] = -1;
		exits [6] [WEST] = 1;

	}//end of rooms
}//end of class