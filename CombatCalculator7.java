import java.util.Scanner;

public class CombatCalculator7{
	public static void main(String[] args){
		int goblinsHealth = 100; //goblin's health
		int goblinsAttackPower = 15; //goblin's attack power
		
		int yourHealth = 100; //player's health
		int yourAttackPower = 12; //player's attack power
		int yourMagicPower = 0; //player's magic power
		
		int number;
		int option = 0;
		Scanner inputReader = new Scanner(System.in);
		
			boolean loopControl = true;
				while (loopControl){
					
					//game intro
					System.out.println("                    ");
					System.out.println("You are fighting a goblin!");
					System.out.println("                    ");
					System.out.println(" ********************");
					System.out.println(" Goblin's Health = " + goblinsHealth);
					System.out.println(" ********************");
					System.out.println("                    ");
					System.out.println(" ~~~~~~~~~~~~~~~~~~~~");
					System.out.println(" Your Health = " + yourHealth);
					System.out.println(" Your Magic Power = " + yourMagicPower);
					System.out.println(" ~~~~~~~~~~~~~~~~~~~~");
					
					// combat menu
					System.out.println("                    ");
					System.out.println(" Combat Options: ");
					System.out.println("1.) Sword Attack");
					System.out.println("2.) Cast Spell");
					System.out.println("3.) Charge Mana");
					System.out.println("4.) Run Away");
					System.out.println("                    ");
					
					// fight promt
					System.out.println("What will you choose?");
					
					
				number = inputReader.nextInt();
				
					if(number == 1){
						
						goblinsHealth = goblinsHealth - yourAttackPower;
						yourHealth = yourHealth - 10;
						System.out.println("You strike the Goblin with your sword for 12 points of damage");
						System.out.println(" *************************************************************");
						System.out.println("                    ");
						System.out.println("                    ");
						System.out.println("                    ");
					} else if(number == 2){
						
						if(yourMagicPower >= 3){
							goblinsHealth = goblinsHealth / 2;
							yourMagicPower = yourMagicPower - 3;
							yourHealth = yourHealth - 10;
							System.out.println("You cast the weaken spell on the monster.");
							System.out.println(" ******************************************");
							System.out.println("                    ");
							System.out.println("                    ");
							System.out.println("                    ");
						} else{
							
							System.out.println("You don't have enough mana.");
							System.out.println(" ****************************");
							System.out.println("                    ");
							System.out.println("                    ");
							System.out.println("                    ");
						}
					} else if(number == 3){
						
						yourMagicPower = yourMagicPower + 1;
						yourHealth = yourHealth - 10;
						System.out.println(" You focus and charge your magic power");
						System.out.println(" **************************************");
						System.out.println("                    ");
						System.out.println("                    ");
						System.out.println("                    ");
					} else if(number == 4){
						
						loopControl = false;
						System.out.println(" You run away");
						System.out.println(" **************");
						System.out.println("                    ");
						System.out.println("                    ");
						System.out.println("                    ");
						break;
					} else {
						
						System.out.println(" I don't understand that command");
						System.out.println(" ********************************");
						System.out.println("                    ");
						System.out.println("                    ");
						System.out.println("                    ");
					}
					
					
					if(goblinsHealth <= 0 || yourHealth <= 0){
						
						loopControl = false;
						System.out.println("                      ");
						System.out.println("                    ");
						System.out.println("                    ");
						System.out.println(" *****************************");
						System.out.println(" *  You defeated the goblin! *");
						System.out.println(" *****************************");
						System.out.println("                    ");
						System.out.println("                    ");
						System.out.println("                    ");
					} else{
						
						loopControl = true;
					}
				}
		}
}