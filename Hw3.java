import java.util.Scanner; //import scanner
import java.util.Random; //import random


public class Hw3{
	private static int yourHealth = 0; //player's health
	private static int yourAttackPower = 0; //player's attack power
	private static int yourMagicPower = 0;//player's magic power
	
	
	private static int monstersAttackPower =  0;//monster's attak
	private static int monstersHealth = 0;//monster's health
	private static int monstersXP = 0;//onsters experience
	private static String name = "Monster's name" ;//monster name

		
		
	public static void main(String[] args){
		
		createHero();//running hero method
		createMonster();//running monster method
		runCombat();//running ccombat method
		
		
	}//end of main
	
	private static void createHero(){
		int yourPoints = 20;//points player starts with
		int pointsSpent = 0;// points player spends
		//int yourHealth = 0; //player's health
		//int yourAttackPower = 0; //player's attack power
		//int yourMagicPower = 0;//player's magic power
			
			while(yourPoints > 0){
			//intro stats
				System.out.println("attack power = " + yourAttackPower + "   ||   health = " + yourHealth + "   ||   magic power = " + yourMagicPower);
				System.out.println(" 1) +10 health");
				System.out.println(" 2) +1	attack	power");
				System.out.println(" 3) +3	magic	power");
				System.out.println(" You have " + yourPoints + " points to spend: " + pointsSpent);

				Scanner input = new Scanner(System.in);//scanner
				int userChoice = 0;// declare scanner variable
			


				userChoice = input.nextInt(); 
					if(userChoice == 1){
						yourHealth = yourHealth + 10;//edd points to health
						yourPoints = yourPoints - 1;//subtract your points
						pointsSpent = pointsSpent + 1;//add your points
					} else if(userChoice == 2){
						yourAttackPower = yourAttackPower + 1;//edd points to attack
						yourPoints = yourPoints - 1;//subtract your points
						pointsSpent = pointsSpent + 1;//add your points
					} else if(userChoice == 3){
						yourMagicPower = yourMagicPower + 3;//edd points to magic
						yourPoints = yourPoints - 1;//subtract your points
						pointsSpent = pointsSpent + 1;//add your points
					} else{
						System.out.println("Your input is invalid... Try again");//invalid input
					}
			}//end of while

	}//end of hero
	
	private static void createMonster(){
		Random monster = new Random();// random number gen
		int monsterName;// declare monsters name
		
		for (int counter = 0; counter <= 0; counter++){
				monsterName = monster.nextInt(3);
				if(monsterName == 0){
					monstersAttackPower = 8;//reasigning variable value
					monstersHealth = 75;//reasigning variable value
					monstersXP = 1;	//reasigning variable value
					name = "GOBLIN"; // reasigning monsters name
					//System.out.println("You are fighting a " + name + "!");//asign 0 to goblin
					//System.out.println("Attack Power: " + monstersAttackPower + "   ||   Monster's Health: " + monstersHealth + "   ||   Monster's Experiance: " + monstersXP);
					
				} else if(monsterName == 1){
					monstersAttackPower = 12;//reasigning variable value
					monstersHealth = 100;//reasigning variable value
					monstersXP = 3;//reasigning variable value
					name = "ORC"; // reasigning monsters name
					//System.out.println("You are fighting a " + name + "!");//asign 1 to orc
					//System.out.println("Attack Power: " + monstersAttackPower + "   ||   Monster's Health: " + monstersHealth + "   ||   Monster's Experiance: " + monstersXP);
				} else{
					monstersAttackPower = 15;//reasigning variable value
					monstersHealth = 150;//reasigning variable value
					monstersXP = 5;//reasigning variable value
					name = "TROLL"; // reasigning monsters name
					//System.out.println("You are fighting a " + name + "!");//asign 2 to troll
					//System.out.println("Attack Power: " + monstersAttackPower + "   ||   Monster's Health: " + monstersHealth + "   ||   Monster's Experiance: " + monstersXP);
				}
				
			}//end of random
	}//end of createMonster
	
	private static void runCombat(){

		//combat
		boolean loopControl = true;
			while (loopControl){
				
				System.out.println("----------------------------------");//aesthetic
				System.out.println("I will now print out your stats");//test
				System.out.println("health = " + yourHealth + "   ||   attack power = " + yourAttackPower + "   ||   magic power = " + yourMagicPower);//test
				System.out.println("----------------------------------");//aesthetic
				System.out.println("I will now print out monsters stats");//test
				System.out.println("You are fighting the " + name + "!");
				System.out.println("Attack Power: " + monstersAttackPower + "   ||   Monster's Health: " + monstersHealth + "   ||   Monster's Experiance: " + monstersXP);//test
				
				System.out.println("----------------------------------");//aesthetic
				System.out.println("----------------------------------");//aesthetic
				System.out.println("----------------------------------");//aesthetic
				System.out.println(" Combat Options: ");
				System.out.println("1.) Sword Attack");
				System.out.println("2.) Cast Spell");
				System.out.println("3.) Charge Mana");
				System.out.println("4.) Run Away");
				System.out.println("                    ");
							
				// fight promt
				System.out.println("What will you choose?");
				
				Scanner input = new Scanner(System.in);//scanner
				int userChoice = 0;// declare scanner variable
					//user input
					userChoice = input.nextInt();
					
						if(userChoice == 1){
							swordAttack();
						} else if(userChoice == 2){
							castSpell();
						} else if(userChoice == 3){
							chargeMana();
						} else if(userChoice == 4){
							runAway();
							loopControl = false;
							break;
						}else{
							System.out.println("Wrong answer... try again");
						}//end of option
					//health loop
					if(monstersHealth <= 0){//if monster dies
						
						loopControl = false;
						System.out.println("                      ");
						System.out.println("                    ");
						System.out.println("                    ");
						System.out.println(" *****************************");
						System.out.println("   You defeated the " + name + "!");
						System.out.println(" *****************************");
						System.out.println("                    ");
						System.out.println("                    ");
						System.out.println("                    ");
					} else if(yourHealth  <= 0) {
						loopControl = false;//if you die
						System.out.println("                      ");
						System.out.println("                    ");
						System.out.println("                    ");
						System.out.println(" *****************************");
						System.out.println("           YOU LOSE");
						System.out.println(" *****************************");
						System.out.println("                    ");
						System.out.println("                    ");
						System.out.println("                    ");
					}else{//while both are alive
						
						loopControl = true;
			}//end loop
			}
	}//end of run camobat
	
	private static void swordAttack(){//sword method
		Random damage = new Random();//random
		int damageDone;
		damageDone = 1 + damage.nextInt(yourAttackPower);
			monstersHealth = monstersHealth - damageDone;//changing monsters health
			System.out.println("You use your sword to attack the " + name + "!");
			System.out.println("The " + name + " has lost: " + damageDone + " points!");
			System.out.println("The " + name + " health is: " + monstersHealth);
			System.out.println("----------------------------------");//aesthetic
			System.out.println("----------------------------------");//aesthetic
		
		Random attack = new Random();
		int monsterAttack;
		monsterAttack = attack.nextInt(monstersAttackPower);
			yourHealth = yourHealth - monsterAttack;//changing your health
			System.out.println("The " + name + " has attacked you");
			System.out.println("You have lost: " + monsterAttack + " points!");
			System.out.println("Your health is: " + yourHealth);
	}//end of swordAttack
	
	private static void castSpell(){//cast spell
		if(yourMagicPower >= 3){
			monstersHealth = monstersHealth / 2;//changing monsters health
			yourMagicPower = yourMagicPower - 3;
			System.out.println("You cast a spell on " + name);
			System.out.println("The " + name + " health is: " + monstersHealth);
			System.out.println("----------------------------------");//aesthetic
			System.out.println("----------------------------------");//aesthetic
			
			Random attack = new Random();
			int monsterAttack;
			monsterAttack = attack.nextInt(monstersAttackPower);
				yourHealth = yourHealth - monsterAttack;//changing your health
				System.out.println("The " + name + " has attacked you");
				System.out.println("You have lost: " + monsterAttack + " points!");
				System.out.println("Your health is: " + yourHealth);
			
		} else{
			System.out.println("You do not have enough mana");
		}
	}
	
	private static void chargeMana(){//charge mana 
		yourMagicPower = yourMagicPower + 3;
		System.out.println("you magic power: " + yourMagicPower);
		
		Random attack = new Random();
			int monsterAttack;
			monsterAttack = attack.nextInt(monstersAttackPower);
				yourHealth = yourHealth - monsterAttack;//changing your health
				System.out.println("The " + name + " has attacked you");
				System.out.println("You have lost: " + monsterAttack + " points!");
				System.out.println("Your health is: " + yourHealth);
		
	}//end of mana
	
	private static void runAway(){
		System.out.println("You ran away, coward");
	}//end of run away
}//end of class

